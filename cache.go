package cache

import (
	"fmt"
	"time"
)

var (
	provider CacheProvider
)

//CacheProvider interface defines required set of methods to be implemented by inheritor
type CacheProvider interface {
	Get(key string, value interface{}) error
	Set(key string, value interface{}, expiry time.Duration) error
	Remove(key string) error
	Close()
}

//CacheProviderConfig interface defines required set of methods to be implemented by inheritor
type CacheProviderConfig interface {
	LoadConfiguration(connectionstring string) error
	CreateProvider() (CacheProvider, error)
}

//Initialize initializes caching facade
func Initialize(configuration CacheProviderConfig) error {

	var err error
	provider, err = configuration.CreateProvider()
	return err
}

//Provider returns singleton provider instance
func Provider() (CacheProvider, error) {
	if provider == nil {
		return nil, fmt.Errorf("Cache provider non-initialized")
	}
	return provider, nil
}
