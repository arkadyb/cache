package couchbase

import (
	"cache"
	"testing"
	"time"

	assert "github.com/stretchr/testify/assert"
)

func TestLoadConfig(t *testing.T) {
	cfg := new(CacheProviderConfig)

	var configString = "{\"servers\":[\"http://localhost:8091\"],\"bucket\":\"default\"}"
	err := cfg.LoadConfiguration(configString)
	assert.NoError(t, err)
	assert.Equal(t, []string{"http://localhost:8091"}, cfg.Servers)
	assert.Equal(t, "default", cfg.Bucket)
}

func TestLoadConfigInvalidConfig(t *testing.T) {
	cfg := new(CacheProviderConfig)

	var configString = "{\"servers\":[\"http://localhost:8091\"],,\"bucket\":\"default\"}"
	err := cfg.LoadConfiguration(configString)
	assert.Error(t, err, "Unexpected error")
}

func TestCreateProvider(t *testing.T) {
	cfg := new(CacheProviderConfig)

	var configString = "{\"servers\":[\"http://localhost:8091\"],\"bucket\":\"default\"}"
	err := cfg.LoadConfiguration(configString)
	assert.NoError(t, err)
}

func TestCreateProviderNoServers(t *testing.T) {
	cfg := new(CacheProviderConfig)

	var configString = "{\"bucket\":\"default\"}"

	err := cfg.LoadConfiguration(configString)
	assert.NoError(t, err)
	p, err := cfg.CreateProvider()
	assert.Error(t, err)
	assert.Nil(t, p)
}

func TestCreateProviderMultipleServersDifferentSchema(t *testing.T) {
	cfg := new(CacheProviderConfig)
	var configString = "{\"servers\":[\"http://localhost:8091\",\"https://localhost2:8091\"],\"bucket\":\"default\"}"
	err := cfg.LoadConfiguration(configString)
	assert.NoError(t, err)
	_, err = cfg.CreateProvider()
	assert.Error(t, err, "Unexpected error")
}

func TestClose(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		pp.Close()
	}
}

func TestSet(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		err = pp.Set("key", "value", time.Minute)
		assert.NoError(t, err)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestGet(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		var value string
		err := pp.Get("key", &value)
		assert.NoError(t, err)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRemove(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		err := pp.Remove("key")
		assert.NoError(t, err)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRetrieveString(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		err = pp.Set("key", "value", time.Minute)
		assert.NoError(t, err)
		var value string
		err = pp.Get("key", &value)
		assert.NoError(t, err)
		assert.True(t, value == "value")
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRetrieveStruct(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		err = pp.Set("key", getTestStruct(), time.Minute)
		assert.NoError(t, err)
		var value testStruct
		err = pp.Get("key", &value)
		assert.NoError(t, err)
		assert.Equal(t, getTestStruct(), value)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRetrieveBinary(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		var value = []byte{byte('A'), byte('A'), byte('A'), byte('A'), byte('B')}
		err = pp.Set("key", value, time.Minute)
		assert.NoError(t, err)
		var out []byte
		err = pp.Get("key", &out)
		assert.NoError(t, err)
		assert.Equal(t, value, out)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRetrieveStringPointer(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		var value = new(string)
		*value = "test"
		err = pp.Set("key", value, time.Minute)
		assert.NoError(t, err)
		var out string
		err = pp.Get("key", &out)
		assert.NoError(t, err)
		assert.Equal(t, *value, out)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRetrieveStringPointerToInterface(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		var value = new(string)
		*value = "test"
		err = pp.Set("key", value, time.Minute)
		assert.NoError(t, err)
		var out interface{}
		err = pp.Get("key", &out)
		outstring := out.(string)
		assert.NoError(t, err)
		assert.Equal(t, *value, outstring)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRetrieveArrayPointer(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		var array = []byte{byte('A'), byte('A'), byte('A'), byte('A'), byte('B')}
		var value = &array
		err = pp.Set("key", value, time.Minute)
		assert.NoError(t, err)
		var out []byte
		err = pp.Get("key", &out)
		assert.NoError(t, err)
		assert.Equal(t, *value, out)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRetrieveArrayWithError(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		var array = []byte{byte('A'), byte('A'), byte('A'), byte('A'), byte('B')}
		var value = &array
		err = pp.Set("key", value, time.Minute)
		assert.NoError(t, err)
		var out int
		err = pp.Get("key", &out)
		assert.Error(t, err)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRetrieveStringWithError(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		var value = "test"
		err = pp.Set("key", value, time.Minute)
		assert.NoError(t, err)
		var out int
		err = pp.Get("key", &out)
		assert.Error(t, err)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRetrieveEmptyInterface(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		var testS = getTestStruct()
		var intface interface{} = testS
		var value = &intface
		err = pp.Set("key", value, time.Minute)
		assert.NoError(t, err)
		var out testStruct
		err = pp.Get("key", &out)
		assert.NoError(t, err)
		assert.Equal(t, *value, out)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestRetrieveInterface(t *testing.T) {
	pp, err := setupCacheProvider(t)
	if assert.NoError(t, err) {
		var testS = getTestStruct()
		var value = &testS
		err = pp.Set("key", value, time.Minute)
		assert.NoError(t, err)
		var out testStruct
		err = pp.Get("key", &out)
		assert.NoError(t, err)
		assert.Equal(t, *value, out)
		assert.NotPanics(t,
			pp.Close,
		)
	}
}

func TestDecode(t *testing.T) {
	//prepare
	zt := new(ZlibTranscoder)
	data := getTestStruct()
	compressed, mode, err := zt.Encode(&data)
	assert.NoError(t, err)
	assert.NotNil(t, compressed)
	testcases := []struct {
		name    string
		success bool
		mode    uint32
		data    interface{}
		err     error
	}{
		{"Success", true, mode, &data, nil},
	}
	//run
	for _, testcase := range testcases {
		t.Run(testcase.name, func(t *testing.T) {
			err := zt.Decode(compressed, testcase.mode, testcase.data)
			if testcase.success {
				assert.NoError(t, err)
			} else {
				assert.Error(t, err)
				assert.EqualError(t, err, testcase.err.Error())
			}
		})
	}
}

func setupCacheProvider(t *testing.T) (cache.CacheProvider, error) {
	cfg := new(CacheProviderConfig)

	var configString = "{\"servers\":[\"http://localhost:8091\",\"http://localhost:8091\"],\"bucket\":\"default\"}"
	err := cfg.LoadConfiguration(configString)
	assert.NoError(t, err)
	p, err := cfg.CreateProvider()
	if assert.NoError(t, err) {
		return p, nil
	}

	return nil, err
}

func getTestStruct() testStruct {
	return testStruct{
		"test",
		1,
		false,
		new(int),
		[]string{"test", "test"},
	}
}

type testStruct struct {
	Field1 string   `xml:"field1"`
	Field2 int      `xml:"field2"`
	Field3 bool     `xml:"field3"`
	Field4 *int     `xml:"field4"`
	Field5 []string `xml:"field5"`
}
