package couchbase

import (
	"bytes"
	"compress/zlib"
	"cache"
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"time"

	"github.com/couchbase/gocb"
)

func (pc *CacheProviderConfig) url() (string, error) {
	if pc.Servers == nil {
		return "", fmt.Errorf("No servers defined")
	}
	var (
		schema string
	)

	urls := make([]*url.URL, len(pc.Servers))
	for i, server := range pc.Servers {
		u, err := url.Parse(server)
		if err != nil {
			return "", err
		}
		if schema != "" && schema != u.Scheme {
			return "", fmt.Errorf("Different schema for node, expeted %s, got %s", schema, u.Scheme)
		}
		if schema == "" {
			schema = u.Scheme
		}
		urls[i] = u
	}

	fullURL := schema + "://"
	for i, url := range urls {
		fullURL += url.Host
		if i < len(urls)-1 {
			fullURL += ";"
		}
	}
	return fullURL, nil
}

//CacheProviderConfig is implementation of cache.CacheProviderConfig
type CacheProviderConfig struct {
	Servers []string `json:"servers"`
	Bucket  string   `json:"bucket"`
}

//LoadConfiguration is mapping values from connection string into configuration object
func (pc *CacheProviderConfig) LoadConfiguration(data string) error {
	var config CacheProviderConfig
	err := json.Unmarshal([]byte(data), &config)
	if err != nil {
		return err
	}

	pc.Bucket = config.Bucket
	pc.Servers = config.Servers

	return nil
}

//CreateProvider does initial setup
func (pc *CacheProviderConfig) CreateProvider() (cache.CacheProvider, error) {
	var (
		provider CacheProvider
		err      error
	)

	provider.url, err = pc.url()
	provider.bucket = pc.Bucket
	provider.cbCluster, err = gocb.Connect(provider.url)
	if err != nil {
		return nil, err
	}

	provider.cbBucket, err = provider.cbCluster.OpenBucket(provider.bucket, "")
	if err != nil {
		return nil, err
	}
	secureZlibTranscoder, err := NewTranscoder()
	if err != nil {
		return nil, err
	}
	provider.cbBucket.SetTranscoder(secureZlibTranscoder)

	return &provider, nil
}

//CacheProvider implements cache.CacheProvider
type CacheProvider struct {
	url    string
	bucket string

	cbCluster *gocb.Cluster
	cbBucket  *gocb.Bucket
}

//Get implements cache.CacheProvider.Get
func (p *CacheProvider) Get(key string, value interface{}) error {
	_, err := p.cbBucket.Get(key, value)

	if err != nil {
		return err
	}

	return nil
}

//Set implements cache.CacheProvider.Set method
func (p *CacheProvider) Set(key string, value interface{}, expiry time.Duration) error {
	_, err := p.cbBucket.Upsert(key, value, uint32(expiry.Seconds()))
	return err
}

//Remove implements cache.CacheProvider.Remove method
func (p *CacheProvider) Remove(key string) error {
	_, err := p.cbBucket.Remove(key, 0)
	return err
}

//Close implements cache.CacheProvider.Close method
func (p *CacheProvider) Close() {
	p.cbBucket.Close()
}

//ZlibTranscoder implements transcoder interface using zlib compression
type ZlibTranscoder struct {
}

//NewTranscoder returns new zlib transcoder ref
func NewTranscoder() (*ZlibTranscoder, error) {
	transcoder := new(ZlibTranscoder)
	return transcoder, nil
}

//Decode implements Transcoder.Decode method
func (t ZlibTranscoder) Decode(inbytes []byte, flags uint32, out interface{}) error {

	err := decompress(inbytes, out)
	if err != nil {
		return err
	}

	return nil
}

func decompress(inbytes []byte, out interface{}) error {

	var buf bytes.Buffer
	_, err := buf.Write(inbytes)
	if err != nil {
		return err
	}

	r, err := zlib.NewReader(&buf)
	if err != nil {
		return err
	}
	defer r.Close()

	var unBuf bytes.Buffer
	_, err = io.Copy(&unBuf, r)
	if err != nil {
		return err
	}

	err = json.Unmarshal(unBuf.Bytes(), out)
	if err != nil {
		return err
	}

	return nil
}

//Encode implements Trascoder.Encode method
func (t ZlibTranscoder) Encode(value interface{}) ([]byte, uint32, error) {

	bObj, err := json.Marshal(value)
	if err != nil {
		return nil, 0, err
	}

	return compress(bObj), 3 << 24, nil
}

func compress(inbytes []byte) []byte {

	var compressedData bytes.Buffer
	w := zlib.NewWriter(&compressedData)

	_, err := w.Write(inbytes)
	if err != nil {
		return nil
	}
	w.Close()

	return compressedData.Bytes()
}
