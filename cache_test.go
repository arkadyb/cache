package cache

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type dummyCacheProviderConfig struct {
}

func (d *dummyCacheProviderConfig) CreateProvider() (CacheProvider, error) {
	p := new(dummyCacheProvider)
	return p, nil
}

func (d *dummyCacheProviderConfig) LoadConfiguration(connectionstring string) error {
	return nil
}

type dummyCacheProvider struct {
}

func (p *dummyCacheProvider) Get(key string, value interface{}) error {
	return nil
}

func (p *dummyCacheProvider) Set(key string, value interface{}, expiry time.Duration) error {
	return nil
}

func (p *dummyCacheProvider) Remove(key string) error {
	return nil
}

func (p *dummyCacheProvider) Close() {

}

func TestInitialize(t *testing.T) {
	config := new(dummyCacheProviderConfig)
	err := config.LoadConfiguration("{\"servers\":[\"http://localhost:8091\"],\"bucket\":\"default\"}")
	assert.NoError(t, err)
	err = Initialize(config)
	assert.NoError(t, err)
	p, err := Provider()
	assert.NotNil(t, p)
	assert.NoError(t, err)
	provider = nil
}

func TestNoInitialize(t *testing.T) {
	config := new(dummyCacheProviderConfig)
	err := config.LoadConfiguration("{\"servers\":[\"http://localhost:8091\"],\"bucket\":\"default\"}")
	assert.NotNil(t, config)
	assert.NoError(t, err)
	p, err := Provider()
	assert.Nil(t, p)
	assert.Error(t, err)
	provider = nil
}
