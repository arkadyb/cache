package main

import (
	"cache"
	"cache/couchbase"
	"fmt"
	"time"
)

func main() {
	cbConfig := new(couchbase.CacheProviderConfig)
	err := cbConfig.LoadConfiguration("{\"servers\":[\"http://localhost:8091\"],\"bucket\":\"default\"}")
	if err != nil {
		fmt.Println(err.Error())
	}

	err = cache.Initialize(cbConfig)
	if err != nil {
		fmt.Println(err.Error())
	}

	provider, err := cache.Provider()
	if err != nil {
		fmt.Println(err.Error())
	}

	fiveMinutes, _ := time.ParseDuration("5m")

	provider.Set("key", "value", fiveMinutes)

	var get string
	err = provider.Get("key", &get)

	if err != nil {
		fmt.Println(err.Error())
	}

	fmt.Println(get)
}
